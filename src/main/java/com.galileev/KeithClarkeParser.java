package com.galileev;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;

import static com.galileev.TokenType.*;

/**
 * Uses Keith Clarke's expression climbing
 */
public class KeithClarkeParser implements Parser {
  public KeithClarkeParser(String input) {
    lexer = new Lexer(new StringReader(input));
  }

  int priority(TokenType tokenType) {
    switch (tokenType) {
      case PLUS:
      case MINUS:
        return 1;
      case MUL:
      case DIV:
        return 2;
      case EQ:
        return 0;

    }
    return -1;
  }

  boolean isRightAssoc(TokenType tokenType) {
    return tokenType == EQ;
  }

  public String expr() throws IOException, ParseException {
    String parse = expr(0);
    expect(EOF);
    lexer.yyclose();
    return parse;
  }

  String expr(int priority) throws IOException, ParseException {
    String l = parsePrimary();

    advance();

    int p;
    while ((p = priority(tokenType)) >= priority) {
      String op = tokenType.name();
      String parse = expr(isRightAssoc(tokenType) ? p : p + 1);
      l = String.format("[%s %s %s]", op, l, parse);
    }

    return l;
  }

  private String parsePrimary() throws IOException, ParseException {
    advance();

    if (LPAREN == tokenType) {
      String l = expr(0);
      expect(RPAREN);
      return l;
    } else if (tokenType == NUMBER || tokenType == IDENTIFIER) {
      return lexer.yytext();
    } else if (tokenType == STRING) {
      return "'" + lexer.text() + "'";
    } else if (tokenType == MINUS) {
      return "(- " + parsePrimary() + ")";
    } else {
      fail("'(' | NUMBER | STRING | IDENTIFIER");
    }
    throw new IllegalStateException();
  }

  private void fail(String message) throws ParseException {
    throw new ParseException(message, lexer.column());
  }

  private TokenType advance() throws IOException {
    return (tokenType = lexer.yylex());
  }

  private void expect(TokenType e) throws ParseException {
    if (tokenType != e)
      fail("Expected: " + e + " got: " + tokenType);
  }

  private Lexer lexer;
  private TokenType tokenType;
}
