package com.galileev;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.EnumSet;
import java.util.Stack;

import static com.galileev.TokenType.*;

public class ShuntingYard implements Parser {
  public ShuntingYard(String s) throws IOException {
    lexer = new Lexer(new StringReader(s));
  }

  public String expr() throws IOException, ParseException {
    Stack<TokenType> stack = new Stack<>();
    Stack<FunctionCall> functionCalls = new Stack<>();
    Stack<String> out = new Stack<>();

    scan(stack, functionCalls, out);

    if (!functionCalls.empty())
      fail(functionCalls.pop() + "()");

    while (!stack.empty()) {
      TokenType pop = stack.pop();
      if (pop == LPAREN)
        fail("missing )");
      out(pop, out);
    }

    assert out.size() == 1;

    return out.pop();
  }

  private void scan(Stack<TokenType> stack, Stack<FunctionCall> functionCalls, Stack<String> out)
      throws IOException, ParseException {
    scan:
    while (advance() != EOF) {
      //ugly, but java lacks goto :(
      //Normally the body is executed only once, except for IDENTIFIER.
      while (true) {
        switch (tokenType) {
          case NUMBER:
            out.push(lexer.yytext());

            break;
          case IDENTIFIER:
            String value = lexer.yytext();

            if (advance() != LPAREN) {
              out.push(value);
            } else {
              stack.push(IDENTIFIER);
              stack.push(LPAREN);
              FunctionCall functionCall = new FunctionCall(value);
              functionCalls.push(functionCall);
              switch (advance()) {
                case EOF:
                  fail("Expected ) or an argument.");
                  return;
                case RPAREN:
                  break;
                default:
                  functionCall.arity = 1;
              }
            }

            if (tokenType == EOF) return;

            continue;
          case STRING:
            out.push(lexer.text());
            break;
          case LPAREN:
            stack.push(LPAREN);
            break;
          case RPAREN:
            while (!stack.empty()) {
              TokenType top = stack.pop();
              if (top == LPAREN) {
                if (!stack.empty() && stack.peek() == IDENTIFIER) {
                  stack.pop();
                  FunctionCall functionCall = functionCalls.pop();
                  functionCall(functionCall.name, functionCall.arity, out);
                }
                continue scan;
              }
              out(top, out);
            }
            fail("mismatched )");
            break;
          case ARGDELIM:
            if (stack.empty() || functionCalls.empty() || out.empty())
              fail("misplaced ,");

            functionCalls.peek().arity += 1;

            while (!stack.empty()) {
              TokenType top = stack.pop();
              if (top == LPAREN) {
                stack.push(LPAREN);
                continue scan;
              }
              out(top, out);
            }
            break;
          default:
            // operator
            boolean right = isRightAssoc(tokenType);
            int p = precedence(tokenType);
            while (!stack.empty()) {
              TokenType top = stack.peek();
              int k = precedence(top);
              if (k > p || (k == p && !right)) {
                stack.pop();
                out(top, out);
              } else break;
            }
            stack.push(tokenType);
        }

        continue scan;
      }
    }
  }

  private void out(TokenType top, Stack<String> out) throws ParseException {
    if (EnumSet.of(PLUS, MINUS, MUL, DIV, EQ).contains(top)) {
      if (out.size() < 2)
        fail("missing arg: " + top.name());
      else
        functionCall(top, 2, out);
    } else
      out(top);
  }

  private void functionCall(Object function, int arity, Stack<String> out) {
    String s = "";
    for (int i = 0; i < arity; i++) {
      s = " " + out.pop() + s;
    }

    out.push("[" + function + s + "]");
  }

  private void out(Object o) {
    System.out.println(o);
  }

  int precedence(TokenType tokenType) {
    switch (tokenType) {
      case PLUS:
      case MINUS:
        return 1;
      case MUL:
      case DIV:
        return 2;
      case EQ:
        return 0;

    }
    return -1;
  }

  boolean isRightAssoc(TokenType tokenType) {
    return tokenType == EQ;
  }

  private void fail(String message) throws ParseException {
    throw new ParseException(message, lexer.column());
  }

  private TokenType advance() throws IOException {
    return (tokenType = lexer.yylex());
  }

  private final Lexer lexer;
  private TokenType tokenType;
}

class FunctionCall {
  FunctionCall(String name) {
    this.name = name;
  }

  String name;
  int arity;

  @Override
  public String toString() {
    return name + "/" + arity();
  }

  int arity() {
    return arity;
  }
}