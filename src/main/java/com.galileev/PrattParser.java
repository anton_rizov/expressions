package com.galileev;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static com.galileev.TokenType.*;

public class PrattParser implements Parser {
  public PrattParser(String s) {
    lexer = new Lexer(new StringReader(s));
    prefixParsers = new HashMap<>();
    PrefixParser prefixParser = new PrefixParser();
    for (TokenType e : new TokenType[]{PLUS, MINUS, LPAREN, NUMBER, STRING, IDENTIFIER}) {
      prefixParsers.put(e, prefixParser);
    }

    infixParsers = new HashMap<>();
    infixr(EQ, 0);

    infixl(PLUS, 1);
    infixl(MINUS, 1);
    infixl(MUL, 2);
    infixl(DIV, 2);
  }

  void infixr(TokenType tokenType, int precedence){
    infix(tokenType, precedence, true);
  }

  void infixl(TokenType tokenType, int precedence){
    infix(tokenType, precedence, false);
  }

  void infix(TokenType tokenType, int precedence, boolean right) {
    infixParsers.put(tokenType, new InfixParser2(precedence, right));
  }

  public String expr() throws IOException, ParseException {
    return parse(0);
  }

  String parse(int precedence) throws IOException, ParseException {
    advance();
    String left = parsePrefix();
    advance();
    do {
      InfixParser infixParser = infixParsers.get(tokenType);
      if (infixParser == null || infixParser.precedence(tokenType) < precedence) break;
      left = infixParser.parse(tokenType, left, this);
    } while (true);
    return left;
  }

  private String parsePrefix() throws IOException, ParseException {
    PrefixParser parser = prefixParsers.get(tokenType);
    if (parser == null)
      throw new ParseException("unexpected tokenType: " + tokenType, lexer.column());
    return parser.parse(tokenType, this, lexer);
  }

  private void fail(String message) throws ParseException {
    throw new ParseException(message, lexer.column());
  }

  private TokenType advance() throws IOException {
    return (tokenType = lexer.yylex());
  }

  void expect(TokenType e) throws ParseException {
    if (tokenType != e)
      fail("Expected: " + e + " got: " + tokenType);
  }

  private Lexer lexer;
  private TokenType tokenType;

  private Map<TokenType, PrefixParser> prefixParsers;
  private Map<TokenType, InfixParser> infixParsers;
}

class PrefixParser {
  public String parse(TokenType tokenType, PrattParser parser, Lexer lexer) throws IOException, ParseException {
    switch (tokenType) {
      case IDENTIFIER:
      case NUMBER:
        return lexer.yytext();
      case STRING:
        return lexer.text();
      case PLUS:
        return "(+ " + parser.parse(0) + ")";
      case MINUS:
        return "(- " + parser.parse(0) + ")";
      case LPAREN:
        String s = parser.parse(0);
        parser.expect(RPAREN);
        return s;
    }
    throw new ParseException("unexpected tokenType " + tokenType, lexer.column());
  }
}

class InfixParser {
  public String parse(TokenType tokenType, String left, PrattParser parser) throws IOException, ParseException {
    String op = tokenType.name();
    String parse = parser.parse(precedence(tokenType) + (isRightAssoc(tokenType) ? 0 : 1));
    return String.format("[%s %s %s]", op, left, parse);
  }

  boolean isRightAssoc(TokenType tokenType) {
    return tokenType == EQ;
  }

  public int precedence(TokenType tokenType) {
    switch (tokenType) {
      case PLUS:
      case MINUS:
        return 1;
      case MUL:
      case DIV:
        return 2;
      case EQ:
        return 0;
    }
    return -1;
  }
}

class InfixParser2 extends InfixParser {
  int precedence;
  boolean right;

  InfixParser2(int precedence, boolean right) {
    this.precedence = precedence;
    this.right = right;
  }

  public String parse(TokenType tokenType, String left, PrattParser parser) throws IOException, ParseException {
    String op = tokenType.name();
    String parse = parser.parse(precedence + (right ? 0 : 1));
    return String.format("[%s %s %s]", op, left, parse);
  }

  public int precedence(TokenType tokenType) {
    return precedence;
  }
}
