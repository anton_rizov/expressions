package com.galileev;

public enum TokenType {
  PLUS, MINUS, MUL, DIV,

  LPAREN, RPAREN,

  STRING, NUMBER, IDENTIFIER,

  EQ,

  ARGDELIM,

  EOF
}
