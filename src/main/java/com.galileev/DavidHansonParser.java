package com.galileev;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;

import static com.galileev.TokenType.*;

/**
 * David R. Hanson, "Compact Recursive-descent Parsing of Expressions"
 */
class DavidHansonParser implements Parser {
  private static final int LEFT = 1;
  private static final int RIGHT = 0;

  private static final TokenType[][] PRECEDENCE = {
      {EQ},
      {MINUS, PLUS},
      {MUL, DIV},
  };

  private static final int[] ASSOCIATIVITY = {RIGHT, LEFT, LEFT};

  public DavidHansonParser(String s) throws IOException {
    lexer = new Lexer(new StringReader(s));
  }

  public String expr() throws IOException, ParseException {
    return expr(EOF);
  }

  private String expr(TokenType expected) throws IOException, ParseException {
    advance();
    String s = expr(0);
    expect(expected);
    return s;
  }

  private String expr(int k) throws IOException, ParseException {
    if (k >= PRECEDENCE.length) {
      String s = primary();
      advance();
      return s;
    }

    String p = expr(k + 1);

    while (isOp(tokenType, k)) {
      String op = tokenType.name();
      advance();
      String s = expr(k + ASSOCIATIVITY[k]);
      p = String.format("[%s %s %s]", op, p, s);
    }
    return p;
  }

  private boolean isOp(TokenType tokenType, int k) {
    for (TokenType e : PRECEDENCE[k])
      if (e == tokenType)
        return true;

    return false;
  }

  private String primary() throws IOException, ParseException {
    if (LPAREN == tokenType) {
      return expr(RPAREN);
    } else if (tokenType == NUMBER || tokenType == IDENTIFIER) {
      return lexer.yytext();
    } else if (tokenType == STRING) {
      return "'" + lexer.text() + "'";
    } else if (tokenType == MINUS) {
      return "(- " + expr() + ")";
    } else {
      fail("( | NUMBER | STRING | IDENTIFIER");
    }
    throw new IllegalStateException();
  }

  private void fail(String message) throws ParseException {
    throw new ParseException(message, lexer.column());
  }

  private TokenType advance() throws IOException {
    return (tokenType = lexer.yylex());
  }

  private void expect(TokenType e) throws ParseException {
    if (tokenType != e)
      fail("Expected: " + e + " got: " + tokenType);
  }

  private final Lexer lexer;
  private TokenType tokenType;
}
