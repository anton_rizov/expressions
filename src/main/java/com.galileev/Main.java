package com.galileev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

public class Main {
  private static final Class[] PARSER_CLASSES = new Class[]{KeithClarkeParser.class, PrattParser.class, ShuntingYard.class, DavidHansonParser.class};

  public static void main(String[] args) throws Exception {
    test("x");
    test("x=y");
    test("x= 1 +2");
    test("x = 1 + 2 = a");
    test("x = y = 1 +2 = c = d");
    test("(a)=1");
    test("x = y = 1+2+3-4 = (a+b)/2");
    testException("1 + ");
    //ShuntingYard currently doesn't support unary minus
    test("(- [PLUS 1 2])", "-(1+2)", allBut(ShuntingYard.class));
    test("[MUL 1 (- 2)]", "1*-2", allBut(ShuntingYard.class));
  }

  private static void test(String expression) throws Exception {
    String parsed = parse(expression, PARSER_CLASSES[0]);
    test(parsed, expression, PARSER_CLASSES);
  }

  private static void test(String parsed, String expression, Class<?>... classes) throws Exception {
    for (Class<?> c : classes) {
      assertEquals(parsed, parse(expression, c));
    }
  }

  private static String parse(String expression, Class<?> c) throws Exception {
    Constructor<?> constructor = c.getConstructor(String.class);
    Object o = constructor.newInstance(expression);
    return ((Parser) o).expr();
  }

  private static void assertEquals(String a, String b) {
    if (!a.equals(b))
      throw new RuntimeException(a + "<>" + b);
  }

  private static void testException(String expression) throws Exception {
    testException(expression, PARSER_CLASSES);
  }

  private static void testException(String expression, Class<?>... classes) throws Exception {
    for (Class<?> c : classes) {
      try {
        parse(expression, c);
        throw new RuntimeException("Expected exception not thrown.");
      } catch (ParseException ignore) {
      }
    }
  }

  private static Class<?>[] allBut(Class<?>... exclude) {
    int n = PARSER_CLASSES.length - exclude.length;
    Set<Class<?>> excludeSet = new HashSet<>(asList(exclude));
    Class<?>[] result = new Class[n];
    int i = 0;
    for (Class c : PARSER_CLASSES) {
      if (!excludeSet.contains(c))
        result[i++] = c;
    }
    return result;
  }

  @SuppressWarnings("UnusedDeclaration")
  private void interactive() throws IOException, ParseException {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    String line;
    while (true) {
      System.out.print("expr: ");
      line = in.readLine();
      if (line == null || "".equals(line))
        return;
      new ShuntingYard(line).expr();
      new PrattParser(line).expr();
    }
  }
}

