package com.galileev;

import java.io.IOException;
import java.text.ParseException;

public interface Parser {
  String expr()  throws IOException, ParseException;
}
