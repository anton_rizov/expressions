package com.galileev;

import static com.galileev.TokenType.*;

%%
%class Lexer
%unicode
%line
%column
%type com.galileev.TokenType
%eofval{
    return EOF;
%eofval}

%{
  StringBuilder text = new StringBuilder();

  public String text() { return text.toString(); }

  public int column() {return yycolumn;}
  public int line() {return yyline;}
%}

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

Comment        = "//" {InputCharacter}* {LineTerminator}?

Identifier     = [:jletter:] [:jletterdigit:]*

NUM            = {FloatLit} {Exponent}? | NaN | Infinity

FloatLit       = {FloatLit1} | {FloatLit2}
FloatLit1      = [0-9]+ ("." [0-9]*)?
FloatLit2      = "." [0-9]+
Exponent       = [eE][+-]?[0-9]+

%state STRING

%%
<YYINITIAL> {
  {NUM}                  { return NUMBER; }

  {Identifier}           { return IDENTIFIER; }

  "'"                    { text.setLength(0); yybegin(STRING); }

  "="                    { return EQ; }
  "+"                    { return PLUS; }
  "-"                    { return MINUS; }
  "*"                    { return MUL; }
  "/"                    { return DIV; }
  "("                    { return LPAREN; }
  ")"                    { return RPAREN; }
  ","                    { return ARGDELIM;}


  {Comment}              { /* ignore */ }

  {WhiteSpace}           { /* ignore */ }
}

<STRING> {
  "'"                    { yybegin(YYINITIAL); return TokenType.STRING; }
  [^\n\r\'\\]+           { text.append( yytext() ); }
  \\t                    { text.append('\t'); }
  \\n                    { text.append('\n'); }

  \\r                    { text.append('\r'); }
  \\\'                   { text.append("'"); }
  \\                     { text.append('\\'); }
}

/* error fallback */
.|\n                     { throw new Error("Illegal character <"
                                           + yytext() + ">"); }
